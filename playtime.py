# Playtime → Percentage of Average Lifespan

while True:
    games = input("Enter the number of games you would like to calculate: ")
    if games.isdigit():
        games = int(games)
        break
    else:
        print("Please enter a number!\n")

i = 1
average_life = 641471.2

while i <= games:
    game_name = input("Name of Game: ")
    playtime = input("Playtime in Hours: ")
    pt_decimal = float(playtime) / average_life
    pt_percentage = f"{pt_decimal:.2%}"
    print(f"{game_name}: {pt_percentage}\n")
    i += 1
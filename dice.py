import random
import time

one = 0
two = 0
three = 0
four = 0
five = 0
six = 0

rolls = int(input("Roll for how many times? "))
total_rolls = rolls

while rolls != 0:
  number = random.randint(1, 6)
  if number == 1:
    one += 1
  elif number == 2:
    two += 1
  elif number == 3:
    three += 1
  elif number == 4:
    four += 1
  elif number == 5:
    five += 1
  elif number == 6:
    six += 1

  print(number)
  rolls -= 1
  if rolls != 0:
    time.sleep(0.5)

print(f"\nOne was rolled {one}/{total_rolls} times")
print(f"Two was rolled {two}/{total_rolls} times")
print(f"Three was rolled {three}/{total_rolls} times")
print(f"Four was rolled {four}/{total_rolls} times")
print(f"Five was rolled {five}/{total_rolls} times")
print(f"Six was rolled {six}/{total_rolls} times")
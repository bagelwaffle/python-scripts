# Python Scripts

A collection of random Python scripts that I have created

| Script | Function |
| ------ | -------- |
| [dice.py](./dice.py) | dice rolls |
| [playtime.py](./playtime.py) | game playtime to percentage of average lifespan |
